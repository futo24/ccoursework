#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


// Define an appropriate struct
typedef struct {
    unsigned int year;
    unsigned char month;
    unsigned char day;
    unsigned char hour;
    unsigned char minute;
} TIME;

typedef struct {
    TIME time;
    unsigned int steps;
} FITNESS_DATA;


// Define any additional variables here
// https://stackoverflow.com/questions/5919996/how-to-detect-reliably-mac-os-x-ios-linux-windows-in-c-preprocessor
#if defined(WIN32)
const char DATA_FILE_PATH[] = "..\\data\\FitnessData_2023.csv";
#else
const char DATA_FILE_PATH[] = "./data/FitnessData_2023.csv";
#endif

const char DATE_DELIM[] = "-";
const char TIME_DELIM[] = ":";
const char CSV_DELIM[] = ",";

// Global
FITNESS_DATA *Records = NULL;


// A simple way to check the validation of a number.
int _safeConvert(char *src, int *dst, const char *format) {
    if (src == NULL) return -1;
    char *buf = malloc(strlen(src) + 1);
    int result = atoi(src);
    sprintf(buf, format, result);
    // Avoid using strcmp, because the string may end with '\n'
    if (strncmp(buf, src, strlen(buf)) != 0) return -1;
    free(buf);
    *dst = result;
    return 0;
}

int safeConvert(char *src, int *dst) {
    return _safeConvert(src, dst, "%d");
}

int safeConvertPadZero(char *src, int *dst) {
    return _safeConvert(src, dst, "%02d");
}

int fillDate(FITNESS_DATA *record, char *date) {
    // Fill year and check validation
    char *yearStr = strtok(date, DATE_DELIM);
    int year;
    if (safeConvert(yearStr, &year) == -1) return -1;

    // Fill month and check validation
    char *monthStr = strtok(NULL, DATE_DELIM);
    int month;
    if (safeConvertPadZero(monthStr, &month) == -1) return -1;
    if (month > 12 || month < 0) return -1;

    // Fill day and check validation
    char *dayStr = strtok(NULL, DATE_DELIM);
    int day;
    if (safeConvertPadZero(dayStr, &day) == -1) return -1;
    if (day == 0) return -1;
    if (day > 31 || day < 0) return -1;

    record->time.year = year;
    record->time.month = month;
    record->time.day = day;

    return 0;
}

int fillTime(FITNESS_DATA *record, char *time) {
    // Fill hour and check validation
    char *hourStr = strtok(time, TIME_DELIM);
    if (hourStr == NULL) return -1;
    int hour;
    if (safeConvertPadZero(hourStr, &hour) == -1) return -1;
    if (hour > 23 || hour < 0) return -1;

    // Fill minute and check validation
    char *minuteStr = strtok(NULL, TIME_DELIM);
    if (minuteStr == NULL) return -1;
    int minute;
    if (safeConvertPadZero(minuteStr, &minute) == -1) return -1;
    if (minute > 59 || minute < 0) return -1;

    record->time.hour = hour;
    record->time.minute = minute;

    return 0;
}

void processFile(const char *fileName) {
    extern int errno;

    // Read file
    FILE *file = fopen(fileName, "r");
    if (!file) {
        printf("[Error] Failed to open file: %s\n", strerror(errno));
        exit(1);
    }

    // Get the total count of records
    int recordCount = 1;
    char ch;
    while (1) {
        // If the last line does end with '\n', recordCount should be reduced by 1.
        if (feof(file)) {
            if (ch == '\n') recordCount--;
            break;
        }
        ch = fgetc(file);
        if (ch == '\n') recordCount++;
    }
    
    // Allocate records
    Records = (FITNESS_DATA *)malloc(recordCount * sizeof(FITNESS_DATA));
    
    // Reset file pointer
    fseek(file, 0, SEEK_SET);

    // Read file line by line
    int count = 0;
    char buf[1024];
    while (fgets(buf, 1024, file) != NULL) {
        // Split by ','
        char *dateStr = strtok(buf, CSV_DELIM);
        char *timeStr = strtok(NULL, CSV_DELIM);
        char *stepsStr = strtok(NULL, CSV_DELIM);

        // Fill records.
        int steps;
        if (fillDate(&Records[count], dateStr) == -1 ||
            fillTime(&Records[count], timeStr) == -1 ||
            safeConvert(stepsStr, &steps) == -1
        ) {
            printf("[Error] Invalid record on line %d.\n", count + 1);
            exit(1);
        }
        Records[count].steps = steps;

        count++;
    }
}

void previewRecords(int nums) {
    for (int i = 0; i < nums; i++) {
        // YYYY-MM-DD/HH:MM/STEPS
        printf("%d-%02d-%02d/%02d:%02d/%d\n",
            Records[i].time.year,
            Records[i].time.month,
            Records[i].time.day,
            Records[i].time.hour,
            Records[i].time.minute,
            Records[i].steps
        );
    }
}

// Complete the main function
int main() {
    processFile(DATA_FILE_PATH);
    previewRecords(3);
    free(Records);
    return 0;
}
